#include <gb/gb.h>

typedef struct Character {
    //Sprites Order:
    //TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT, B_L_GUN, B_R_GUN
    UBYTE downSprites[6];
    UBYTE leftSprites[6];
    UBYTE upSprites[6];
    UBYTE rightSprites[6];
    BYTE charId;
    BYTE hasGun;
    UINT8 xPos;
    UINT8 yPos;
    BYTE direction;

} Character;

void assignSprites(Character* character) {
    UBYTE sprites[6];
    BYTE gunOffset = 0;
    if (character->hasGun) {
        gunOffset = 2;
    }

    if(character->direction == 0) {
        sprites[0] = character->downSprites[0];
        sprites[1] = character->downSprites[1];
        sprites[2] = character->downSprites[2];
        sprites[3] = character->downSprites[3];
        sprites[4] = character->downSprites[4];
        sprites[5] = character->downSprites[5];
    }
    else if(character->direction == 1) {
        sprites[0] = character->leftSprites[0];
        sprites[1] = character->leftSprites[1];
        sprites[2] = character->leftSprites[2];
        sprites[3] = character->leftSprites[3];
        sprites[4] = character->leftSprites[4];
        sprites[5] = character->downSprites[5];
    }
    else if(character->direction == 2) {
        sprites[0] = character->upSprites[0];
        sprites[1] = character->upSprites[1];
        sprites[2] = character->upSprites[2];
        sprites[3] = character->upSprites[3];
        sprites[4] = character->upSprites[4];
        sprites[5] = character->upSprites[5];
    }
    else if(character->direction == 3) {
        sprites[0] = character->rightSprites[0];
        sprites[1] = character->rightSprites[1];
        sprites[2] = character->rightSprites[2];
        sprites[3] = character->rightSprites[3];
        sprites[4] = character->rightSprites[4];
        sprites[5] = character->rightSprites[5];
    }

    set_sprite_tile((character->charId) * 4, sprites[0]);
	set_sprite_tile(1 + (character->charId) * 4, sprites[1]);
	set_sprite_tile(2 + (character->charId) * 4, sprites[2 + gunOffset]);
	set_sprite_tile(3 + (character->charId) * 4, sprites[3 + gunOffset]);

    move_sprite((character->charId) * 4, character->xPos, character->yPos);
    move_sprite(1 + (character->charId) * 4, character->xPos + 8, character->yPos);
    move_sprite(2 + (character->charId) * 4, character->xPos, character->yPos + 8);
    move_sprite(3 + (character->charId) * 4, character->xPos + 8, character->yPos + 8);
}