#include <gb/gb.h>
#include <stdio.h>
#include <gb/font.h>
#include "System.c"
#include "Character.c"
#include "charSprites.c"
#include "mapData/outdoorsBG.c"
#include "mapData/testMap.c"

Character player;

void setupChar(Character* character, BYTE charId) {
	character->charId = charId;
	character->hasGun = 0;
	character->xPos = 32;
	character->yPos = 32;
	character->direction = 0;

	character->downSprites[0] = 1;
	character->downSprites[1] = 2;
	character->downSprites[2] = 3;
	character->downSprites[3] = 4;
	character->downSprites[4] = 0;
	character->downSprites[5] = 0;

	character->leftSprites[0] = 9;
	character->leftSprites[1] = 10;
	character->leftSprites[2] = 11;
	character->leftSprites[3] = 12;
	character->leftSprites[4] = 0;
	character->leftSprites[5] = 0;

	character->upSprites[0] = 5;
	character->upSprites[1] = 6;
	character->upSprites[2] = 7;
	character->upSprites[3] = 8;
	character->upSprites[4] = 0;
	character->upSprites[5] = 0;

	character->rightSprites[0] = 13;
	character->rightSprites[1] = 14;
	character->rightSprites[2] = 15;
	character->rightSprites[3] = 16;
	character->rightSprites[4] = 0;
	character->rightSprites[5] = 0;
	
	assignSprites(character, 0);
}

void look(Character* character, BYTE direction) {
	character->direction = direction;
	assignSprites(character);
}
void move(Character* character, INT8 x, INT8 y){
	character->xPos = x;
	character->yPos = y;
	assignSprites(character);
}

void setUpFont() {
	font_t font;
	font_init();
	font = font_load(font_min);
	font_set(font);
}

void main() {
	setUpFont();

	set_sprite_data(0, 100, charSprites);

	set_bkg_data(0,100, outdoorsBG);
	set_bkg_tiles(0,0,20,18,testMap);

	setupChar(&player, 0);

	//set_win_tiles(0,0,5,1, );

	DISPLAY_ON;
	SHOW_SPRITES;
	SHOW_WIN;
	SHOW_BKG;

	

	while(1){
		if(joypad() & J_DOWN) {
			look(&player, 0);
			move(&player, player.xPos, player.yPos + 8);
		}
		if(joypad() & J_LEFT) {
			look(&player, 1);
			move(&player, player.xPos - 8, player.yPos);
		}
		if(joypad() & J_UP) {
			look(&player, 2);
			move(&player, player.xPos, player.yPos - 8);
		}
		if(joypad() & J_RIGHT) {
			look(&player, 3);
			move(&player, player.xPos + 8, player.yPos);
		}

		delay(100);
	}
}
